Rounded corners
---------------

speedwm versions before 1.9 had built in rounded corner support done using standard X11 libraries. This worked relatively well, but only for clients, not the bar. In 1.9 however, this was removed. The removal was done because if you like rounded corners, chances are you already have a compositor like `picom` installed on your system anyway.

`picom` does rounded corners for any program, and that includes the speedwm bar, and it does it better than speedwm did, so it made no sense to keep it around in speedwm causing possible future bugs.

### Rounded corners using `picom`

To enable rounded corners in speedwm, install picom.

For Gentoo: `emerge --ask picom`

For Arch: `pacman -S picom`

Once `picom` is installed, you can add `picom &` to `~/.config/speedwm/autostart.sh` which will allow it to automatically run on speedwm startup. Alternatively, add it to another file which is autostarted, such as `~/.xinitrc`.

`picom` has a configuration file in `~/.config/picom/picom.conf`. Here you can do a bunch of cool things, but importantly you can enable rounded corners. A good start would be to add the following to the file:

`corner-radius = 10.0;`

`rounded-corners-exclude = [`

`"class_g = 'Dunst'",`

`];`

`round-borders = 10;`

`round-borders-exclude = [`

`#"class_g = 'Chromium'",`

`];`

You can adjust `round-borders` and `corner-radius` depending on how sharp you want the window borders. Similar to speedwm rules, you can exclude certain clients based on class. There are a few examples here by default. Settings should apply immediately when the file is saved which is very convenient. In case it doesn't though, restart `picom`.

### Excluding the speedwm bar

Some users may not want to round the bar, specifically if you're not using any barpadding. In that case, simply add a rule like this:

`rounded-corners-exclude = [`

`"class_g = 'speedwm'",`

`];`

to your `picom.conf` configuration file.

### speedwm configuration

It may be a good idea to disable window borders in the speedwm configuration. This isn't required of course, but window borders can look a bit out of place. Otherwise, you may want to consider increasing the size. Add `speedwm.border.size: 0` to `~/.config/speedwm/speedwmrc` to disable the window border for clients in speedwm.

It also does not make much sense to use rounded corners without padding, so I set outer horizontal and outer vertical padding to 5 like so:

`speedwm.bar.paddingoh: 5`

`speedwm.bar.paddingov: 5`

I also want a smaller bar, but instead of decreasing the font size, I choose to decrease bar height by 2 like so:

`speedwm.bar.height: -2`

Gaps are set to 10 by default, but because 10 pixels of padding is too much, I choose to decrease inner and outer gaps like this:

`speedwm.gaps.sizeih: 5`

`speedwm.gaps.sizeiv: 5`

`speedwm.gaps.sizeoh: 5`

`speedwm.gaps.sizeov: 5`

My configuration looks like this. Feel free to copy it to your own `speedwmrc`.

`speedwm.border.size: 0`

`speedwm.bar.paddingoh: 5`

`speedwm.bar.paddingov: 5`

`speedwm.bar.height: -2`

`speedwm.gaps.sizeih: 5`

`speedwm.gaps.sizeiv: 5`

`speedwm.gaps.sizeoh: 5`

`speedwm.gaps.sizeov: 5`
