Client rules
============

The `Rule` array allows you to specify rules for X11 clients based on factors such as WM_CLASS and WM_NAME. In the English language, that means you can specify how clients are tiled based on the class, instance or title of said client.

To get this information, you can use the X11 utility `xprop` to get information about a client. If you want to see in detail how the rule system works, refer to function `applyrules` and the `Rule` struct in `speedwm.c`.

### Rule structure (user-friendly)

1. Class

Type: `const char *`

2. Instance

Type: `const char *`

3. Title

Type: `const char *`

4. Tags

Type: `unsigned int`

5. isfloating

Type: `int`

6. ispermanent

Type: `int`

7. isterminal

Type: `int`

8. noswallow

Type: `int`

9. Monitor

Type: `int`

10. unmanaged

Type: `int`

11. ignoretransient

Type: `int`

12. floatx

Type: `int`

13. floaty

Type: `int`

14. floatw

Type: `int`

15. floath

Type: `int`

16. scratchkey

Type: `const char`

### Rule structure (internally)

`typedef struct {`

`const char *class;`

`const char *instance;`

`const char *title;`

`unsigned int tags;`

`int isfloating;`

`int ispermanent;`

`int isterminal;`

`int noswallow;`

`int monitor;`

`int unmanaged;`

`int ignoretransient;`

`int floatx, floaty, floatw, floath;`

`const char scratchkey;`

`} Rule;`

### Class

Every X11 client has a class. Even the window manager, speedwm in this case which has the class `speedwm` has a class. speedwm is **not** responsible for setting a client's class, clients using X11 libraries should set this themselves. speedwm just retrieves this information when a client is mapped.

As mentioned previously, you can get the class for a client by running the command-line utility `xprop` on any client. Running `xprop` on speedwm's bar for example will return:

`
[anon@arch ~]$ xprop
WM_CLASS(STRING) = "speedwm", "speedwm"
`

Running it and clicking on the root window however will return information such as the different tags and their glyphs, supported atoms, loaded xrdb resources and information such as the focused client which is not relevant here.

Now, some clients may have multiple classes, `st` for example has both `st` and `St`. Note the class of the program you want to use. Let's say, we want to set a rule for Chromium. Running `xprop | grep CLASS` returns the following information.

`
[anon@arch ~]$ xprop | grep CLASS
WM_CLASS(STRING) = "chromium", "Chromium"
`

This means our class here is `chromium`. That would also be the value of `const char *class` or simply the class. This means if the class matches what we specified (ie. `chromium`), and provided the instance and title also matches an existing client, we set the other values to what is specified.

### Instance

This allows us to specify which instance of the class
