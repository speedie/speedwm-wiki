Restoring the dwm bar
=====================

This wiki article describes how to restore the default dwm look to the bar. This should look nearly identical if not exactly the same as dwm-6.4, and can be done in like 2 minutes.

### Restoring the dwm modules

To do this, you must first restore the original modules dwm uses. To do this, you can edit `bar.h`. However, what I recommend you do is copy `bar.h` to `bar.rl.h`. The `rl` headers will be compiled into the binary without replacing the original files and so is nice if you want a configuration without pushing it to the Git repository. Of course, this is optional.

In `bar.rl.h`, you'll want to remove any modules not present in default dwm such as all the powerline modules and systray. You can choose to keep things like the systray if you want, but this article is about being as close to regular dwm as possible.

If you want, you can copy my `barrules` array below:

`{ -1,       0,     bar_align_left,   width_tags,            draw_tags,            click_tags,          0,          "tags" },`

`{ -1,       0,     bar_align_left,   width_ltsymbol,        draw_ltsymbol,        click_ltsymbol,      0,          "layout" },`

`{ -1,       0,     bar_align_right,  width_status_basic,    draw_status_basic,    click_status_basic,  0,          "status" },`

`{ -1,       0,     bar_align_none,   width_title_basic,     draw_title_basic,     click_title_basic,   0,          "title" },`

As you can see, you'll want to set some of the modules to the `basic` variant. With that done, go ahead and recompile speedwm using `make clean install`.
Then follow along with 'Restoring settings'.

### Restoring settings

Now that we have the old modules, we'll want to restore the colors, fonts, and other things to the dwm settings. This includes disabling gaps, padding, and any other features dwm does not have.

To do this, edit `~/.config/speedwm/speedwmrc`. You *can* add all the .Xresources values yourself, or you can just copy my list of values below into your own config. I should note though that you may not achieve perfect results if your config already has other options. For best results, start with a blank `speedwmrc`.

`speedwm.fonts.font: monospace 10`

`speedwm.gaps.enable: 0`

`speedwm.color.hiddentitle: 0`

`speedwm.color.selectedtitle: 1`

`speedwm.color.layout: 0`

`speedwm.client.map: 0`

`speedwm.client.hide.border: 0`

`speedwm.client.hide.unselected.border: 0`

`speedwm.tiling.resizehints: 1`

`speedwm.bar.alpha: 0`

`speedwm.bar.position: 1`

`speedwm.bar.height: 4`

`speedwm.bar.paddingih: 0`

`speedwm.bar.paddingiv: 0`

`speedwm.bar.paddingoh: 0`

`speedwm.bar.paddingov: 0`

`speedwm.bar.hide.emptytags: 0`

`speedwm.bar.hide.floating: 0`

`speedwm.bar.hide.layout: 0`

`speedwm.bar.hide.status: 0`

`speedwm.bar.hide.title: 0`

`speedwm.bar.hide.clientindicator: 0`

`speedwm.bar.titleposition: 0`

`speedwm.bar.hide.systray: 1`

`speedwm.bar.hide.sticky: 1`

`speedwm.bar.hide.icon: 1`

`speedwm.border.size: 1`

`speedwm.text.layout1: []=`

`speedwm.text.layout2: ><>`

`speedwm.text.layout3: [M]`

`speedwm.text.tag1.empty: 1`

`speedwm.text.tag2.empty: 2`

`speedwm.text.tag3.empty: 3`

`speedwm.text.tag4.empty: 4`

`speedwm.text.tag5.empty: 5`

`speedwm.text.tag6.empty: 6`

`speedwm.text.tag7.empty: 7`

`speedwm.text.tag8.empty: 8`

`speedwm.text.tag9.empty: 9`

`speedwm.text.tag1.used: 1`

`speedwm.text.tag2.used: 2`

`speedwm.text.tag3.used: 3`

`speedwm.text.tag4.used: 4`

`speedwm.text.tag5.used: 5`

`speedwm.text.tag6.used: 6`

`speedwm.text.tag7.used: 7`

`speedwm.text.tag8.used: 8`

`speedwm.text.tag9.used: 9`

`speedwm.col.tag1: #005577`

`speedwm.col.tag2: #005577`

`speedwm.col.tag3: #005577`

`speedwm.col.tag4: #005577`

`speedwm.col.tag5: #005577`

`speedwm.col.tag6: #005577`

`speedwm.col.tag7: #005577`

`speedwm.col.tag8: #005577`

`speedwm.col.tag9: #005577`

`speedwm.col.titlenorm: #222222`

`speedwm.col.titlesel: #005577`

`speedwm.col.titlehid: #222222`

`speedwm.col.windowbordernorm: #444444`

`speedwm.col.windowbordersel: #005577`

`speedwm.col.textnorm: #bbbbbb`

`speedwm.col.textsel: #eeeeee`

`speedwm.status.defaultstatus: dwm-6.4`

Append this to your `speedwmrc` and then simply restart using `libspeedwm --perform core_wm_restart`.

**NOTE: If you're using .Xresources, you'll likely notice that colors are not being overridden. You can stop overriding colors, or you can disable .Xresources support in toggle.h, but do note that doing this means you'll have to edit `options.h` with the values above.**

### Done

After doing the above, you should have a configuration that looks more or less identical to dwm 6.4. If you want to take it a step further, you can disable things like tag previews, window icons and task switcher through `toggle.h` if resources is your concern.
