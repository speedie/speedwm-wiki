Centered status bar
===================

Thanks to [[Configuring the bar]] and the `es` modules, we can position the status text in the center similar to the [splitstatus](https://dwm.suckless.org/patches/splitstatus) patch.

To do this, start off by copying `bar.h` to `bar.rl.h`. This is optional, but I highly recommend it. Now, you'll want to add a module like this.

`{ -1, 0, bar_align_center, width_status_es, draw_status_es, click_status_es, 0, "extra status" },`

We're using `bar_align_center` because we want it in the center, that's the point here. We're using `es` modules because we want to have status text in both the center and the usual right. In the default configuration, you'll (probably) want to remove the title module, or move it to the other bar to get the aesthetic you want. Now that you have the modules configured, compile this using `make clean install`.

To use the `es` modules, you simply add `;` somewhere in the status text. Any text after will be considered extrastatus text. For example `speedwm -s "Hello;World"` will draw Hello as regular status text and World as extra status text in the center of the bar.

### Usage in the status bar

The above is all cool and all, but if we want to use it with the built in status bar, we need to integrate it into the modules. There are many different ways this could be done, but we're going to do it the easiest here. In this example, we're going to put the time in the center and the rest will be in the normal status bar.

Copy `status.h` to `status.rl.h` and `vim status.rl.h`. Now we can remove the `module_time` module, because a duplicate is unnecessary. Let's add a module to the array like this.

`{ ";", "module_time --print", 1, 1 },`

Of course, the `;` here switches to the centered status. Now we can run `make clean install` and restart `speedwm_status`. You should see your expected changes and the time should be in the center now.
