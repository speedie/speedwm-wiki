Welcome to the speedwm wiki!
----------------------------

![image](https://codeberg.org/speedie/speedwm/raw/branch/master/docs/preview.png)

[speedwm](https://speedie.gq/projects/speedwm.php) is a window manager forked
from [dwm](https://dwm.suckless.org) (also known as dynamic window manager).
It manages the user's open windows and tiles them according to a set layout.
This is what makes it dynamic, unlike windows managers like i3 which you may
be used to.

Just like dwm, speedwm also tries to be minimal but also brings functionality
and aesthetics to the table without patching or other work. It is a good
middle ground between creating your own dwm build from scratch and bloated
window managers like awesome which are not primarily configured by editing source code.

Tiling window managers (unlike floating window managers that you may be used to) tile windows based on a set layout making them easy to get productive on. They also encourage the user to use their keyboard instead of the mouse so that the user doesn't have to move his hands much but there are also mouse binds and more can be added by the user if desired.

You have reached the wiki for speedwm, the main source of information
regarding the project. Any information too detailed or complex to be part of the speedwm
release is placed here. Because it's a wiki, anyone may submit changes to the
wiki. The wiki is also free/libre (as in freedom) software and you can have a
copy. See [this git repository](https://codeberg.org/speedie/speedwm-wiki) for
more information.

Not sure where to begin with speedwm? See [[Getting Started]] for a nice way
to get into it!
