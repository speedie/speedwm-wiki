Managing changes
----------------

speedwm's Makefile has a nice feature which allows you to compile one header into the binary while keeping the original unmodified.

If I want to make a change to, let's say `bar.h` in my personal build I just `cp bar.h bar.rl.h`, make changes to `bar.rl.h` and recompile. `bar.h` will be intact, but my changes from bar.rl.h will be applied in the compiled binary.

This feature is currently available for:

- bar.h
- options.h
- keybinds.h
- mouse.h
- status.h
