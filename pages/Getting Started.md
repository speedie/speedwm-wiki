Getting Started
---------------

[speedwm](https://speedie.gq/projects/speedwm.php) is a window manager forked
from [dwm](https://dwm.suckless.org) (also known as dynamic window manager).
It manages the user's open windows and tiles them according to a set layout.
This is what makes it dynamic, unlike windows managers like i3 which you may
be used to.

### This is an X11 window manager, you need to use X.

Yes, this is an X11 window manager. I do not like Wayland and it's
philosophies, and I will not
support it for multiple reasons. See [[Wayland support]] for my reasoning
behind this. Hate me all you want for it!

If it isn't clear, `xorg-server` as well as `xinit` or `sx` is pretty much
expected. You can also use display managers like `sddm` or `lightdm` although
it should be mentioned that `ly` is **known** to have issues with dwm and
therefore also
speedwm. Do note that setting up X11 is out of the scope of this wiki, and you
need to figure out how to do that yourself.

### Supported operating systems

This is not some arbitrary requirement. While it may run if it's not listed
here, I will not be providing support due to having a lack of experience with
said operating system. The main operating systems that **do not** support
speedwm are macOS and Microsoft Windows. UNIX like operating systems are the
most likely to run it.

- Nearly all GNU/Linux distributions
  - NixOS and GNU Guix may require separate instructions. I will not provide
    support for these, use the existing documentation to set it up.
- FreeBSD, OpenBSD, NetBSD
  - There are likely more BSDs that can run speedwm, but I cannot support all
    of them. Try it and see if it works.

### Installation on Gentoo GNU/Linux

Gentoo has slightly different instructions for setting up speedwm. While you
can follow along with the generic installation instructions, I have a Gentoo
overlay with packages for all speedwm versions.

To get started, add [my
overlay](https://codeberg.org/speedie/speedie-overlay). Then simply `emerge
x11-wm/speedwm x11-wm/libspeedwm x11-wm/speedwm-extras`. Optionally, you may
`emerge x11-misc/spde` instead, which is a metapackage for a lot of stuff
including fonts, and general programs that work well for me.

If you want to install speedwm using Git, do this by adding the following
lines:

`=x11-wm/speedwm-9999 **`

`=x11-wm/libspeedwm-9999 **`

`=x11-wm/speedwm-extras-9999 **`

to `/etc/portage/package.accept_keywords` and then emerging the packages as
per the instructions above.

### Installation on generic GNU/Linux distributions

Installing speedwm on most GNU/Linux distributions is very simple. First,
determine if you want to install it using Git (the absolute latest version) or
using a stable tarball.

As of 26/01/2023, 1.8 is the latest tarball release
which is not very stable. For this reason, it is recommended that you `git
clone` the repository. If you do not have `git`, install it using your
distribution's package manager. If you're (still) going to be using a 'stable'
tarball, you do not need to install `git`.

While the list of dependencies will vary depending on the speedwm version
you're going to install (see docs/dependencies.md for your specific version),
below is a list for the latest release, 1.8 and soon to be 1.9.

- libXft
- libXinerama
  - Used for multi-monitor support. It can be disabled if you do not need it
    by commenting the XINERAMA libraries in `toggle.mk`.
- imlib2
  - Used for tag previews and window icons. It can be disabled if you do not
    need it. To do this, comment out `IMLIB2LIBS` in `toggle.mk` and set
    `USEIMLIB2`, `USEWINICON` and `USETAGPREVIEW` to `0` in `toggle.h`.
- pango
- yajl
  - Used for IPC support. It can be disabled if you do not need it. To do
    this, comment out `YAJLINC` and `YAJLLIBS` in `toggle.mk` and set `USEIPC`
    to `0` in `toggle.h`.
- tcc
  - This is a compiler. It is used because it is much faster than `clang` or
    `gcc` (although at the expense of debugging and compatibility). You can
    swap it out by passing `CC=cc` to the `make` command later when we compile
    speedwm.

#### Installation using `git`

This assumes you have `git` installed.

`cd` into a location where your normal user (not root) has permission to both
read and write. Do not clone the speedwm repository in `~/.config` as speedwm
will write configuration files to that location.

**Note: Cloning the repository to `~/.config` is fine
if the speedwm source code directory is **not** `speedwm` but speedwm
will use the `~/.config/speedwm` directory to store configuration files (not
source code).**

From here on, we're going to assume you are in the location `~/Documents`! Run
`git clone https://codeberg.org/speedie/speedwm.git speedwm` to clone the
repository into a directory named `speedwm`. Provided the repository hasn't
moved and you have a working internet connection, you should have received a
copy of the speedwm source code.

You can now `cd speedwm` into it so we can actually install it. If you need to
make changes to `toggle.h`, `toggle.mk`, `host.mk` or any other C source code,
now is the time to do it.

Now, it's time to compile it! Provided you have all dependencies installed,
this should be very, very easy and quick. Run `make clean install` as root to install
speedwm. The binary will be in `/usr/bin/speedwm`. If you need to use a
different compiler (tcc in Arch repositories has a few issues), you can run
`make CC=cc clean install` to use the system default compiler. Alternatively
use `gcc` instead of `cc` or whatever you want.

After installation, it is *highly* recommended that you move your source code
directory to a safe place. speedwm is a source based window manager and
**most** (but not all) changes you make to it are going to be done by
recompiling the window manager. This is, of course done with the same `make
clean install` command you used earlier.

I recommend you do this by forking the speedwm repository, and putting it on a
Git repository somewhere. That way, you can always clone your specific
version.

**Warning: If you lose your source code, you will be
**unable** to make any further changes to speedwm.**

Now follow along with 'Using speedwm'.

#### Installation using a tarball

Installation using a tarball works pretty much the same. Visit [the releases
page](https://codeberg.org/speedie/speedwm/releases) and get the latest
 (or an old if you want) tarball. Avoid the 'Source Code (ZIP)' and 'Source
 Code (TAR.GZ)' options, get the 'speedwm-<version>.tar.gz' file.

`cd` into a location where your normal user (not root) has permission to both
read and write. Make sure `~/.config/speedwm` is not used by anything.

Now we need to unpack the tarball and get our source code. To do this, you can
use the `tar` command. I will spare you the time reading the man page and the
command to unpack the tarball is `tar -xpvf
/path/to/the/speedwm/tarball.tar.gz`. The `-x` argument here means extract.
`-p` means preserve permissions. The `-v` means verbose, we want to see
what's going on. `-f` means file, we want to specify a file to unpack.

After unpacking it, there should be one directory; `speedwm-<version>`. Let's
`cd speedwm-<version>` into it. Now it's time to compile speedwm!

Provided you have all dependencies installed, this should be very, very easy
and quick. Run `make clean install` as root to install speedwm. The binary
will be in `/usr/bin/speedwm`. If you need to use a different compiler (tcc in
Arch repostories has a few issues), you can run `make CC=cc clean install` to
use the system default compiler. Alternatively use `gcc` instead of `cc` or
whatever you want.

After installation, it is *highly* recommended that you move your source code
directory to a safe place. speedwm is a source based window manager and
**most** (but not all) changes you make to it are going to be done by
recompiling the window manager. This is, of course done with the same `make
clean install` command you used earlier.

I recommend you do this by putting the source code on a
Git or subversion repository somewhere. That way, you can always clone your specific
version.

**Warning: If you lose your source code, you will be
**unable** to make any further changes to speedwm.**

Now follow along with 'Using speedwm'.
