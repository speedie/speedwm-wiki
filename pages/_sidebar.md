Basics
======

- [[Home]]
- [[Where do I go?]]
- [[Getting Started]]

### Deeper

- [[Client rules]]
- [[Keybinds]]
- [[Configuring the bar]]
